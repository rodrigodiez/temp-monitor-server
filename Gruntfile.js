module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    env: {
      dev: {
        NODE_ENV: 'development',
      },
      prod: {
        NODE_ENV: 'production',
        DEBUG: ''
      }
    },
    watch: {
      app: {
        files: ['**/*.js', '**/*.jade', 'bin/*', '!**/node_modules/**'],
        tasks: ['forever::restart'],
        options: {
          livereload: true
        }
      }
    },
    forever: {
      app: {
        options: {
          index: 'bin/app',
          command: 'node',
          logDir: 'logs',
          logFile: 'app.out.log',
          errFile: 'app.err.log'      
        },
      },
      api: {
        options: {
          index: 'bin/api',
          command: 'node',
          logDir: 'logs' ,
          logFile: 'api.out.log',
          errFile: 'api.err.log'           
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-forever');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-env');

  grunt.registerTask('dev', ['env:dev', 'forever::start', 'watch:app']);
  grunt.registerTask('prod', ['env:prod', 'forever::start']);

  grunt.registerTask('start', ['forever::start']);
  grunt.registerTask('stop', ['forever::stop']);

  grunt.registerTask('default', ['dev']);
};