$(document).ready(function (){

	var chart = $("#temperatures").highcharts({
        chart: {
            type: 'spline'
        },
        title: false,
        subtitle: false,
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            title: {
                text: 'Temperature (°C)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '°C'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
            {
                name: 'Weather @ SE10 0LE',
                data: temperatures.weather.map(function (element) {
                    return [element.timestamp, element.temperature / 1000];
                })
            }
        ]
	});
});