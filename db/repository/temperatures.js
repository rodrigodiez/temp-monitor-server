var debug = require('debug');
var log = debug('db-repository-temperatures');
var error = debug('db-repository-temperatures:error');
var warn = debug('db-repository-temperatures:warning');

error.log = console.error.bind(console);
warn.log = console.warn.bind(console);

var deferred = require('deferred');
var sqlite3 = require('sqlite3').verbose();

var temperatures = function (db) {

	return {
		getByClientIdAndDateInterval: function(clientId, dateMin, dateMax) {

			log('getByClientIdAndDateInterval', clientId, dateMin, dateMax);

			var def = deferred();

			db.all('SELECT * FROM temperatures WHERE client_id=' + clientId + ' AND timestamp BETWEEN ' + dateMin.getTime() + ' AND ' + dateMax.getTime() + ' ORDER BY timestamp ASC', function (err, rows) {
				if(err) {
					error(err);

					def.reject(err);
				}
				
				def.resolve(rows);
			});

			return def.promise;
		},
		create: function(clientId, temperature) {

			log('create', clientId, temperature);

			var def = deferred();

			db.serialize(function (){
				db.run('PRAGMA foreign_keys = ON');
				db.run('INSERT INTO temperatures  VALUES (1, ' + temperature + ', ' + new Date().getTime() + ')');
			});
		}
	}
}

module.exports = temperatures;