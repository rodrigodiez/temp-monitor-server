#!/usr/bin/env node

require('date-utils');

var debug = require('debug');
var log = debug('db-schema');
var error = debug('db-schema:error');
var warn = debug('db-schema:warning');

error.log = console.error.bind(console);
warn.log = console.warn.bind(console);

var sqlite3 = require('sqlite3').verbose();
var fs = require('fs');

var schema = function (app) {

	var db = new sqlite3.Database(app.get('database file'));

	return {
		create: function() {

			log('Creating schema...');
			db.serialize(function (){
				db.run('PRAGMA foreign_keys = ON');
				db.run('CREATE TABLE clients (id INTEGER PRIMARY KEY, name TEXT)');
				db.run('CREATE TABLE temperatures (client_id REFERENCES clients(id), temperature INTEGER, timestamp INTEGER)');
			});
		},
		drop: function() {

			log('Dropping schema...');
			fs.unlink(app.get('database file'));
		},
		seed: function() {

			log('Inserting clients...');
			db.serialize(function (){
				db.run('INSERT INTO clients VALUES (1, "Weather")');
				db.run('INSERT INTO clients VALUES (2, "Bed room")');
				db.run('INSERT INTO clients VALUES (3, "Living room")');
			});
		}
	}
}

module.exports = schema;