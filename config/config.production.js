var config = require('./config.global');

config.app.http.port = 4000;
config.app.db.path = 'db/sqlite3/production.db';

config.api.http.port = 4001;
config.api.db.path = 'db/sqlite3/production.db';

module.exports = config;