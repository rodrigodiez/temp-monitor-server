var config = require('./config.global');

config.app.http.port = 3000;
config.app.db.path = 'db/sqlite3/development.db';

config.api.http.port = 3001;
config.api.db.path = 'db/sqlite3/development.db';

module.exports = config;