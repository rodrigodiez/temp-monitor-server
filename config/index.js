var env = process.env.NODE_ENV || 'development'
  , config = require('./config.' + env + '.js');

module.exports = config;