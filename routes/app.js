require('date-utils');

var debug = require('debug');
var log = debug('controller-app');
var error = debug('controller-app:error');
var warn = debug('controller-app:warning');

error.log = console.error.bind(console);
warn.log = console.warn.bind(console);

var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();

router.get('/', function(req, res) {

	var db = new sqlite3.Database(req.app.get('database file'));
	var temperatures = require('../db/repository/temperatures')(db);

	var client1Promise = temperatures.getByClientIdAndDateInterval(1, Date.today(), Date.tomorrow());
	
	client1Promise.then(function (result){
		res.render('index', { temperatures: { weather: result }});
	});
});

module.exports = router;
