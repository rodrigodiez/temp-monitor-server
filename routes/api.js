var debug = require('debug');
var log = debug('controller-api');
var error = debug('controller-api:error');
var warn = debug('controller-api:warning');

error.log = console.error.bind(console);
warn.log = console.warn.bind(console);

var express = require('express');
var router = express.Router();
var sqlite3 = require('sqlite3').verbose();

router.post('/temperatures', function(req, res) {
	
	log(req.body);

	var db = new sqlite3.Database(req.app.get('database file'));
	var temperatures = require('../db/repository/temperatures')(db);

	temperatures.create(req.body.apiKey, req.body.temperature);

	res.send('OK');
});

module.exports = router;
